﻿using AutomationFramework.Support.Configuration;
using UI;
using Utility.Shared;
using static AutomationFramework.Support.Constants.Constants;

namespace AutomationFramework.Hook
{
    [Binding]
    public sealed class ReportHooks : ReportHelper
    {
        private static ContextInjection _contextInjection;
        public ReportHooks(ContextInjection contextInjection)
        {
            _contextInjection = contextInjection;
        }

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            ConfigurationReader.Read();
            BeforeTestRunMethod(REPORT_FILE_PATH, "Test_Report");
        }
        [BeforeFeature]
        public static void BeforeFeature(FeatureContext context)
        {
            BeforeFeatureStart(context);
        }
        [BeforeScenario(Order = 2)]
        public void BeforeScenario(ScenarioContext context)
        {
            BeforeScenarioStart(context);
        }
        [AfterStep]
        [Scope(Tag = "API")]
        public void AfterStepForAPI(ScenarioContext context)
        {
            AfterEachStepForAPI();
        }
        [AfterStep]
        [Scope(Tag = "UI")]
        public void AfterStepForUI(ScenarioContext context)
        {
            AfterEachStepForUI(_contextInjection.Driver, SCREENSHOT_FILE_PATH);
        }
        [AfterScenario]
        public static void AfterFeature(FeatureContext context)
        {
            // log.Error(context.TestError.Message);
        }
        [AfterTestRun]
        public static void AfterTestRun()
        {
            AfterTestRunCompletion();
        }
    }
}