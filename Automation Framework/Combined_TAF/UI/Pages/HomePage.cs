﻿using OpenQA.Selenium;

namespace UI.Pages
{
    public class HomePage : BasePage
    {
        public HomePage(IWebDriver driver) : base(driver)
        {
        }
        #region Elemenets
        private readonly By loginOrSingUp = By.XPath("//a[@href='/login']");
        private readonly By loggedInUserName = By.XPath("//i[@class='fa fa-user']/following-sibling::*");
        private readonly By deleteAccountButton = By.XPath("//a[@href='/delete_account']");
        #endregion

        #region Actions
        public LoginAndSignUpPage NavigateToLoginOrSignUpPage()
        {
            FindElement(loginOrSingUp).Click();
            return new LoginAndSignUpPage(driver);
        }
        public string GetLoggedInUserName()
        {
            return FindElement(loggedInUserName).Text.Trim();
        }
        public AccountDeletedPage DeleteAccount()
        {
            FindElement(deleteAccountButton).Click();
            return new AccountDeletedPage(driver);
        }
        #endregion
    }
}