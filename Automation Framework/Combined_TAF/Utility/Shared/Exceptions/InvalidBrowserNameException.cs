﻿namespace Utility.Shared.Exceptions
{
    public class InvalidBrowserNameException : Exception
    {
        public InvalidBrowserNameException(string message) : base(message)
        {

        }
    }
}
