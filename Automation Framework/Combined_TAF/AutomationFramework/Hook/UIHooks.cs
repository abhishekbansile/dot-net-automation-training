﻿using AutomationFramework.Drivers;
using log4net;
using OpenQA.Selenium;
using UI;
using Utility.Shared;

namespace AutomationFramework.Hook
{
    [Binding]
    public sealed class UIHooks
    {
        public ContextInjection _contextInjection { get; set; }
        private static readonly ILog logger = Logger.GetLogger();
        public UIHooks(ContextInjection contextInjection)
        {
            _contextInjection = contextInjection;
        }
        [BeforeScenario(Order = 1)]
        [Scope(Tag = "UI")]
        public void BeforeScenario()
        {
            IWebDriver driver = DriverManager.GetDriver();
            _contextInjection.InitializePages(driver);
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            logger.Info(driver.GetType().Name + " Initialized");
        }
        [AfterScenario]
        [Scope(Tag = "UI")]
        public void TearDown()
        {
            DriverManager.QuitDriver();
            logger.Info("Driver is Quit");
        }
    }
}