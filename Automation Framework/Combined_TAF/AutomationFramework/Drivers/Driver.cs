﻿namespace AutomationFramework.Drivers
{
    public enum Driver
    {
        CHROME,
        FIREFOX,
        EDGE
    }
}
