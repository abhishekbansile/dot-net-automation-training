﻿using AutomationFramework.Drivers;

namespace AutomationFramework.Support.Constants
{
    public static class Constants
    {
        public static string BASE_URL = "https://automationexercise.com/";
        public static string REPORT_FILE_PATH = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\x86\\Debug\\net6.0", "\\TestOutput\\Reports\\ExtentReports\\Report_" + $"{DateTime.Now:yyyy_MM_dd_HH_mm_ss}");
        public static string REPORT_FILE_NAME = "TAF_Report";
        public static string SCREENSHOT_FILE_PATH = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\x86\\Debug\\net6.0", "") + "TestOutput\\Screenshots\\Screenshot_" + $"{DateTime.Now:yyyy_MM_dd_HH_mm_ss}.png";
        public static string RECORDING_FILE_PATH = @"../../../../TestOutput/Recording";
        public static string RECORDING_FILE_NAME = $"Recording_{DateTime.Now:yyyy_MM_dd_HH_mm_ss}.mp4";
        public static bool IS_RECORDING = false;
        // public static string CONFIGURATION_FILE_PATH = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\x86\\Debug\\net6.0", "\\Configuration.json");
        public static string CONFIGURATION_FILE_PATH = @"C:\DotNet Training July\dot-net-training\Automation Framework\Combined_TAF\AutomationFramework\Configuration.json";

        // QA CONFIGURATION
        public static string? USERNAME;
        public static string? PASSWORD;
        public static string? URL;
        // public static bool IS_RECORDING;
        public static bool IS_SCREENSHOT;
        public static int SCREEN_RES_WIDTH;
        public static int SCREEN_RES_HEIGHT;
        public static string? SCREENSHOT_PATH;
        public static string? LOG_PATH;
        public static string? LOG_LEVEL;
        public static Driver BROWSER_TYPE;

    }
}
