﻿using OpenQA.Selenium;

namespace Utility.UI
{
    public static class UIUtils
    {
        public static void CaptureScreenshot(IWebDriver driver, string screenshotPath)
        {
            Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot.SaveAsFile(screenshotPath, ScreenshotImageFormat.Png);
            
        }
    }
}
