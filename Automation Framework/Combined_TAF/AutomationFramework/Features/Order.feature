﻿@API
@DataSource:../TestData/API/PetStore_Order.xlsx 
Feature: Order

    @DataSet:PostOrderWithBody
    Scenario: Place a valid order for a pet
        When I send a Post request to Order end URI with the following data
            | Key      | Value      |
            | id       | <Id>       |
            | petId    | <PetId>    |
            | quantity | <Quantity> |
            | shipDate | <ShipDate> |
            | status   | <Status>   |
            | complete | <Complete> |
        Then the response status code should be OK
        And the response should have the following form data
            | Key      | Value              |
            | id       | <Id>               |
            | petId    | <PetId>            |
            | quantity | <Quantity>         |
            | shipDate | <ExpectedShipDate> |
            | status   | <Status>           |
            | complete | <Complete>         |
 
    @DataSet:PostOrderWithoutBody
    Scenario: Place a order for a pet without body
        When I send a Post request to Order end URI without body
        Then the response status code should be Unsupported Media Type
        And the response should have the following data
            | Key     | Value     |
            | code    | <Code>    |
            | type    | <Type>    |
    
    @DataSet:GetValidOrderById
    Scenario: Find purchase order by Id
        When I send a Post request to Order end URI with the following data
            | Key      | Value      |
            | id       | <Id>       |
            | petId    | <PetId>    |
            | quantity | <Quantity> |
            | shipDate | <ShipDate> |
            | status   | <Status>   |
            | complete | <Complete> |
        Then the response status code should be OK
        And the response should have the following form data
            | Key      | Value              |
            | id       | <Id>               |
            | petId    | <PetId>            |
            | quantity | <Quantity>         |
            | shipDate | <ExpectedShipDate> |
            | status   | <Status>           |
            | complete | <Complete>         |
        When I send a Get request to Order end URI with id <Id>
        Then the response status code should be OK
        And the response should have the following data as response body
            | Key      | Value      |
            | id       | <Id>       |
            | petId    | <PetId>    |
            | quantity | <Quantity> |
            | shipDate | <ExpectedShipDate> |
            | status   | <Status>   |
            | complete | <Complete> |
    
    @DataSet:GetInvalidOrderById
    Scenario: Find a Non-existent Purchase Order by Id
        When I send a Get request to Order end URI with Not-existent Purchase order id <Id>
        Then the response status code should be Not Found
        And the response should have the following data as response  body
            | Key     | Value     |
            | code    | <Code>    |
            | type    | <Type>    |
            | message | <Message> |