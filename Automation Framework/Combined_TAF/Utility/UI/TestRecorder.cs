﻿ /*using ScreenRecorderLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.UI
{
   public class TestRecorder
    {
        private Recorder _recorder;
        private string _outputDirectory;
        private string _outputFile;

        public TestRecorder(string outputDirectory, string outputFile)
        {
            _outputDirectory = outputDirectory;
            _outputFile = outputFile;
        }

        public void StartRecording()
        {
            RecorderOptions options = new RecorderOptions();
            _recorder = Recorder.CreateRecorder(options);

            _recorder.OnRecordingComplete += Recorder_OnRecordingComplete;

            _recorder.Record(Path.Combine(_outputDirectory, _outputFile));
        }

        public void StopRecording()
        {
            _recorder.Stop();
            _recorder.Dispose();
        }

        private void Recorder_OnRecordingComplete(object sender, RecordingCompleteEventArgs e)
        {
            // Handle the recording completion event if needed
        }
    }
}
*/