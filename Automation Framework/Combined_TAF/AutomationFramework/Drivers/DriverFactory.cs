﻿using static AutomationFramework.Support.Constants.Constants;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using Utility.Shared.Exceptions;

namespace AutomationFramework.Drivers
{
    public static class DriverFactory
    {
        public static IWebDriver GetDriver(Driver driver)
        {
            Dictionary<Driver, IWebDriver> drivers = new Dictionary<Driver, IWebDriver>();

            if (drivers.ContainsKey(driver))
            {
                return drivers[driver];
            }

            switch (driver)
            {
                case Driver.CHROME:
                    drivers.Add(driver, new ChromeDriver());
                    return drivers[driver];
                case Driver.FIREFOX:
                    drivers.Add(driver, new FirefoxDriver());
                    return drivers[driver];
                case Driver.EDGE:
                    drivers.Add(driver, new EdgeDriver());
                    return drivers[driver];
                default: throw new BrowserNotFoundException("Provide Valid Browser in Configuration File : " + CONFIGURATION_FILE_PATH);
            }
        }
    }
}
