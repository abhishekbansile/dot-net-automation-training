using NUnit.Framework;
using UI;
using Utility.Shared;

namespace AutomationFramework.StepDefinitions
{
    [Binding]
    public class SignUpStepDefinitions
    {
        private readonly ContextInjection contextInjection;
        private readonly ScenarioContext scenarioContext;
        public SignUpStepDefinitions(ContextInjection contextInjection)
        {
            this.contextInjection = contextInjection;
            scenarioContext = ScenarioContext.Current;
        }
        [When(@"a user enters name and email address on signup page")]
        public void WhenAUserEntersNameAndEmailAddressOnSignupPage(Table table)
        {
            Dictionary<string, string> registrationDetails = DataReader.ConvertTwoColTableToDictionary(table);
            contextInjection.LoginAndSignUpPage.EnterSignUpEmail(registrationDetails["email"]);
            contextInjection.LoginAndSignUpPage.EnterSingUpName(registrationDetails["name"]);
            scenarioContext.Add("email", registrationDetails["email"]);
            scenarioContext.Add("name", registrationDetails["name"]);
        }

        [When(@"a user click on signup button")]
        public void WhenAUserClickOnSignupButton()
        {
            contextInjection.LoginAndSignUpPage.ClickOnSignUpButton();
        }

        [Then(@"verify name and email address in account information section")]
        public void ThenVerifyNameAndEmailAddressInAccountInformationSection()
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(scenarioContext.Get<string>("email"), contextInjection.SignUpDetailsPage.GetEmail());
                Assert.AreEqual(scenarioContext.Get<string>("name"), contextInjection.SignUpDetailsPage.GetName());
            });
        }

        [When(@"a user enters following details in account information section")]
        public void WhenAUserEntersFollowingDetailsInAccountInformationSection(Table table)
        {
            var accountInformation = DataReader.ConvertTwoColTableToDictionary(table);
            contextInjection.SignUpDetailsPage
                .SelectTitle(accountInformation["title"])
                .EnterPassword(accountInformation["password"])
                .SelectDateOfBirth(accountInformation["day"], accountInformation["month"], accountInformation["year"]);
            scenarioContext.Add("AccountInformation", accountInformation);
        }

        [When(@"a user enters following details in address information section")]
        public void WhenAUserEntersFollowingDetailsInAddressInformationSection(Table table)
        {
            var addressInformation = DataReader.ConvertTwoColTableToDictionary(table);
            contextInjection.SignUpDetailsPage.EnterFirstName(addressInformation["firstName"])
                .EnterLastName(addressInformation["lastName"])
                .EnterCompany(addressInformation["company"])
                .EnterAddress1(addressInformation["address1"])
                .EnterAddress2(addressInformation["address2"])
                .EnterState(addressInformation["state"])
                .EnterCity(addressInformation["city"])
                .EnterZipcode(addressInformation["zipcode"])
                .EnterMobileNumber(addressInformation["mobileNumber"]);
            scenarioContext.Add("AddressInformation", addressInformation);
        }

        [When(@"a user click on create account button")]
        public void WhenAUserClickOnCreateAccountButton()
        {
            contextInjection.SignUpDetailsPage.CreateAccount();
        }

        [Then(@"verify that account created successfully page should be visible")]
        public void ThenVerifyThatAccountCreatedSuccessfullyPageShouldBeVisible(Table table)
        {
            var accountCreatedInfo = DataReader.ConvertTwoColTableToDictionary(table);
            Assert.Multiple(() =>
            {
                Assert.AreEqual(accountCreatedInfo["heading"], contextInjection.AccountCreatedPage.GetAccountCreatedHeading());
                Assert.AreEqual(accountCreatedInfo["message"], contextInjection.AccountCreatedPage.GetAccountCreatedMessage());
            });
        }
        [When(@"a user click on delete account button")]
        public void WhenAUserClickOnDeleteAccountButton()
        {
            contextInjection.HomePage.DeleteAccount();
        }
    }
}
