﻿namespace Utility.Shared.Exceptions
{
    public class BrowserNotFoundException : Exception
    {
        public BrowserNotFoundException(string message) : base(message)
        {

        }
    }
}
