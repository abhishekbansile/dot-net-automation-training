using NUnit.Framework;
using log4net;
using UI;
using static AutomationFramework.Support.Constants.Constants;
using Utility.Shared;

namespace AutomationFramework.StepDefinitions
{
    [Binding]
    public class LoginStepDefinitions
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(LoginStepDefinitions));
        private ContextInjection _contextInjection { get; set; }
        public LoginStepDefinitions(ContextInjection contextInjection)
        {
            _contextInjection = contextInjection;
        }

        [Given(@"launch browser with base url")]
        public void GivenLaunchBrowserWithBaseUrl()
        {
            _contextInjection.Driver.Navigate().GoToUrl(BASE_URL);
        }

        [When(@"user clicks on SignUp/Login button")]
        public void WhenUserClicksOnSignUpLoginButton()
        {
            _contextInjection.HomePage.NavigateToLoginOrSignUpPage();
        }

        [When(@"user enters following Email Address and Password in Login to your account Section")]
        public void WhenUserEntersFollowingEmailAddressAndPasswordInLoginToYourAccountSection(Table table)
        {
            Dictionary<string, string> loginDetails = DataReader.ConvertTwoColTableToDictionary(table);
            _contextInjection.LoginAndSignUpPage.EnterLoginEmail(loginDetails["email"]);
            _contextInjection.LoginAndSignUpPage.EnterLoginPassword(loginDetails["password"]);
        }

        [When(@"user click on login button")]
        public void WhenUserClickOnLoginButton()
        {
            _contextInjection.LoginAndSignUpPage.ClickOnLoginButton();
        }

        [Then(@"user should navigate to Home page")]
        public void ThenUserShouldNavigateToHomePage()
        {
            Assert.That(_contextInjection.Driver.Url, Is.EqualTo(BASE_URL));
        }

        [Then(@"validate the Logged in as Name with users Registered Name ""([^""]*)""")]
        public void ThenValidateTheLoggedInAsNameWithUsersRegisteredName(string loggedInUserName)
        {
            Assert.That(_contextInjection.HomePage.GetLoggedInUserName(), Is.EqualTo(loggedInUserName));
        }

        [Then(@"verify error ""([^""]*)"" should be displayed")]
        public void ThenVerifyErrorShouldBeDisplayed(string errorMsg)
        {
            Assert.That(_contextInjection.LoginAndSignUpPage.GetLoginErrorMessage(), Is.EqualTo(errorMsg));
        }

        [Then(@"verify that error message ""([^""]*)"" should be visible on email field on login page")]
        public void ThenVerifyThatErrorMessageShouldBeVisibleOnEmailFieldOnLoginPage(string warningMsg)
        {
            Assert.That(_contextInjection.LoginAndSignUpPage.GetLoginEmailWarningMessage(), Is.EqualTo(warningMsg));
        }

        [Then(@"verify that error message ""([^""]*)"" should be visible on password field on login page")]
        public void ThenVerifyThatErrorMessageShouldBeVisibleOnPasswordFieldOnLoginPage(string warningMsg)
        {
            Assert.That(_contextInjection.LoginAndSignUpPage.GetLoginPasswordWarningMessage(), Is.EqualTo(warningMsg));
        }

        [When(@"a user enters ""([^""]*)"" as email address in Login to your account Section")]
        public void WhenAUserEntersAsEmailAddressInLoginToYourAccountSection(string email)
        {
            _contextInjection.LoginAndSignUpPage.EnterLoginEmail(email);
        }

        [When(@"a user click on login button")]
        public void WhenAUserClickOnLoginButton()
        {
            _contextInjection.LoginAndSignUpPage.ClickOnLoginButton();
        }
    }
}
