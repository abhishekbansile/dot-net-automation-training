﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Utility.UI;

namespace Utility.Shared
{
    public class ReportHelper
    {
        private static ScenarioContext _scenarioContext;
        private static ExtentReports _extentReports;
        private static ExtentHtmlReporter _extentHtmlReporter;
        private static ExtentTest _feature;
        private static ExtentTest _scenario;
        public static void BeforeTestRunMethod(string path, string name)
        {
            _extentHtmlReporter = new ExtentHtmlReporter(path);
            _extentHtmlReporter.Config.ReportName = name;
            _extentHtmlReporter.Config.Theme = Theme.Standard;
            _extentReports = new ExtentReports();
            _extentReports.AttachReporter(_extentHtmlReporter);
        }
        public static void BeforeFeatureStart(FeatureContext featureContext)
        {
            if (featureContext != null)
            {
                _feature = _extentReports.CreateTest<Feature>(featureContext.FeatureInfo.Title, featureContext.FeatureInfo.Description);
            }
        }
        public void BeforeScenarioStart(ScenarioContext scenarioContext)
        {
            if (scenarioContext != null)
            {
                _scenarioContext = scenarioContext;
                _scenario = _feature.CreateNode<Scenario>(scenarioContext.ScenarioInfo.Title, scenarioContext.ScenarioInfo.Description);
            }
        }
        public void AfterEachStepForUI(IWebDriver driver, string screenshotPath)
        {
            ScenarioBlock scenarioBlock = _scenarioContext.CurrentScenarioBlock;

            switch (scenarioBlock)
            {
                case ScenarioBlock.Given:
                    CreateNodeForUI<Given>(driver, screenshotPath);
                    break;
                case ScenarioBlock.When:
                    CreateNodeForUI<When>(driver, screenshotPath);
                    break;
                case ScenarioBlock.Then:
                    CreateNodeForUI<Then>(driver, screenshotPath);
                    break;
                default:
                    CreateNodeForUI<And>(driver, screenshotPath);
                    break;
            }
        }
        public void AfterEachStepForAPI()
        {
            ScenarioBlock scenarioBlock = _scenarioContext.CurrentScenarioBlock;

            switch (scenarioBlock)
            {
                case ScenarioBlock.Given:
                    CreateNodeForAPI<Given>();
                    break;
                case ScenarioBlock.When:
                    CreateNodeForAPI<When>();
                    break;
                case ScenarioBlock.Then:
                    CreateNodeForAPI<Then>();
                    break;
                default:
                    CreateNodeForAPI<And>();
                    break;
            }
        }
        private void CreateNodeForUI<T>(IWebDriver driver, string screenshotPath) where T : IGherkinFormatterModel
        {
            if (_scenarioContext.TestError != null)
            {
                UIUtils.CaptureScreenshot(driver, screenshotPath);
                _scenario.CreateNode<T>(_scenarioContext.StepContext.StepInfo.Text).Fail(_scenarioContext.TestError.Message);
                _scenario.AddScreenCaptureFromPath(screenshotPath);
            }
            else
            {
                _scenario.CreateNode<T>(_scenarioContext.StepContext.StepInfo.Text).Pass("");
            }
        }
        private void CreateNodeForAPI<T>() where T : IGherkinFormatterModel
        {
            if (_scenarioContext.TestError != null)
            {
                _scenario.CreateNode<T>(_scenarioContext.StepContext.StepInfo.Text).Fail(_scenarioContext.TestError.Message);
            }
            else
            {
                _scenario.CreateNode<T>(_scenarioContext.StepContext.StepInfo.Text).Pass("");
            }
        }
        public static void AfterTestRunCompletion()
        {
            _extentReports.Flush();
        }
        public static void AfterScenario()
        {
            if (_scenarioContext.TestError != null)
            {
                string name = _scenarioContext.ScenarioInfo.Title.Replace(" ", "") + ".jpeg";
            }
        }
    }
}
