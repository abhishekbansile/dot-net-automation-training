﻿using Newtonsoft.Json;
using RestSharp;
using System.Net;
using static Utility.API.Constants.PetStore_URI;

namespace Utility.API.Utils
{
    public static class RestClientUtils
    {
        public static RestClient _RestClient = new RestClient(BASE_URL);

        // Return Type - Tuple
        #region Methods

        // Post
        public static (T Body, HttpStatusCode StatusCode) Post<T>(string resource, string payload)
        {
            return ExecuteRequest<T>(resource, Method.Post, payload);
        }

        // Post
        public static (T Body, HttpStatusCode StatusCode) Post<T>(string resource)
        {
            return ExecuteRequest<T>(resource, Method.Post);
        }

        // Get
        public static (T Body, HttpStatusCode StatusCode) Get<T>(string resource)
        {
            return ExecuteRequest<T>(resource, Method.Get);
        }

        // Put
        public static (T Body, HttpStatusCode StatusCode) Put<T>(string resource, string payload)
        {
            return ExecuteRequest<T>(resource, Method.Put, payload);
        }

        // Delete
        public static (T Body, HttpStatusCode StatusCode) Delete<T>(string resource)
        {
            return ExecuteRequest<T>(resource, Method.Delete);
        }

        #endregion

        #region Utility
        private static (T Body, HttpStatusCode) ExecuteRequest<T>(string resource, Method method)
        {
            RestRequest request = CreateRequest(resource, method);
            RestResponse response = _RestClient.Execute(request);
            return (JsonConvert.DeserializeObject<T>(response.Content), response.StatusCode);
        }
        private static (T Body, HttpStatusCode) ExecuteRequest<T>(string resource, Method method, string payload)
        {
            RestRequest request = CreateRequest(resource, method);
            request.AddBody(payload);
            RestResponse response = _RestClient.Execute(request);
            return (JsonConvert.DeserializeObject<T>(response.Content), response.StatusCode);
        }
        
        private static RestRequest CreateRequest(string resource, Method method)
        {
            return new RestRequest(resource, method);
        }
        #endregion

    }
}
