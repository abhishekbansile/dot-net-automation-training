﻿using API.Request.Order;
using API.Response.Order;
using Newtonsoft.Json;
using System.Net;
using Utility.API.Utils;

namespace API.Utils
{
    public class OrderUtils
    {
        #region Return Type - Tuple

        // Place an Order for Pet
        public static (PostOrderValidResponse Body, HttpStatusCode StatusCode) PlaceAnOrder(Dictionary<string, string> orderDetails)
        {
            string orderBody = CreateOrderBody(orderDetails);
            string resource = "/v2/store/order";
            return RestClientUtils.Post<PostOrderValidResponse>(resource, orderBody);
        }

        public static (PostOrderInvalidResponse Body, HttpStatusCode StatusCode) PlaceAnOrder()
        {
            string resource = "/v2/store/order";
            return RestClientUtils.Post<PostOrderInvalidResponse>(resource);
        }

        // Find Purchase Order by Id
        public static (GetOrderValidResponse Body, HttpStatusCode StatusCode) FindPurchaseOrder(int id)
        {
            string resource = $"/v2/store/order/{id}";
            return RestClientUtils.Get<GetOrderValidResponse>(resource);
        }
        // Find Purchase Order by Id
        public static (GetOrderInvalidResponse Body, HttpStatusCode StatusCode) FindNonExistentPurchaseOrder(int id)
        {
            string resource = $"/v2/store/order/{id}";
            return RestClientUtils.Get<GetOrderInvalidResponse>(resource);
        }

        // Delete Purchase Order by Id
        public static (DeleteOrderValidResponse Body, HttpStatusCode StatusCode) DeletePurchaseOrder(int id)
        {
            string resource = $"/v2/store/order/{id}";
            return RestClientUtils.Delete<DeleteOrderValidResponse>(resource);
        }
        #endregion

        private static string CreateOrderBody(Dictionary<string, string> orderDetails)
        {
            PostOrderValidRequest postOrderValidRequest = new PostOrderValidRequest();
            postOrderValidRequest.id = int.Parse(orderDetails["id"]);
            postOrderValidRequest.petId = int.Parse(orderDetails["petId"]);
            postOrderValidRequest.quantity = int.Parse(orderDetails["quantity"]);
            postOrderValidRequest.shipDate = orderDetails["shipDate"];
            postOrderValidRequest.status = orderDetails["status"];
            postOrderValidRequest.complete = bool.Parse(orderDetails["complete"]);
            return JsonConvert.SerializeObject(postOrderValidRequest);
        }
    }
}
