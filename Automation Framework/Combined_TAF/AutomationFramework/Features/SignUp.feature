﻿@UI
@DataSource:../TestData/UI/AutomationExercise.xlsx
Feature: SignUp

Validate Automation Exercise SignUp Functionality

Background: the user launch browser with base Url
    Given launch browser with base url
    When user clicks on SignUp/Login button
  
  @DataSet:RegistrationDetails
  Scenario: Validate new user signup functionality with required as well as optional field
    When a user enters name and email address on signup page
      | Key   | Value   |
      | name  | <Name>  |
      | email | <Email> |
    And a user click on signup button
    Then verify name and email address in account information section
    When a user enters following details in account information section
      | Key      | Value      |
      | title    | <Title>    |
      | password | <Password> |
      | day      | <Day>      |
      | month    | <Month>    |
      | year     | <Year>     |
    And a user enters following details in address information section
      | Key          | Value          |
      | firstName    | <FirstName>    |
      | lastName     | <LastName>     |
      | company      | <Company>      |
      | address1     | <Address1>     |
      | address2     | <Address2>     |
      | country      | <Country>      |
      | state        | <State>        |
      | city         | <City>         |
      | zipcode      | <Zipcode>      |
      | mobileNumber | <MobileNumber> |
    And a user click on create account button
    Then verify that account created successfully page should be visible
      | Key     | Value     |
      | heading | <AccountCreatedHeading> |
      | message | <AccountCreatedMessage> |

  @DataSet:DeleteAccountDetails
  Scenario: Validate delete account functionality
    When user enters following Email Address and Password in Login to your account Section
        | Key      | Value      |
        | email    | <Email>    |
        | password | <Password> |
    And a user click on login button
    Then validate the Logged in as Name with users Registered Name "<Name>"
    When a user click on delete account button
#   Then verify that account deleted should be visible