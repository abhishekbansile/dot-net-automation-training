﻿using TechTalk.SpecFlow;

namespace Utility.Shared
{
    public static class DataReader
    {
        public static Dictionary<string, string> ConvertTwoColTableToDictionary(Table table)
        {
            var dictionary = new Dictionary<string, string>();

            foreach (var row in table.Rows)
            {
                dictionary.Add(row[0], row[1]);
            }
            return dictionary;
        }
    }
}
