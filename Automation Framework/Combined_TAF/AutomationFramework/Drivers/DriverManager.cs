﻿using log4net;
using OpenQA.Selenium;
using Utility.Shared;
using Utility.Shared.Exceptions;

namespace AutomationFramework.Drivers
{
    public class DriverManager
    {
        private static readonly ILog LOGGER = Logger.GetLogger();
        private static object LockObject = new object();
        private static IWebDriver? _driver;
        private static string _driverName;
        private DriverManager()
        {

        }

        public static IWebDriver GetDriver()
        {
            if (_driver == null)
            {
                lock (LockObject)
                {
                    if (_driver == null)
                    {
                        InitializeDriver();
                    }
                }
            }
            return _driver;
        }

        private static void InitializeDriver()
        {
            try
            {
                _driver = DriverFactory.GetDriver(Driver.CHROME);
                _driverName = _driver.GetType().Name;
                LOGGER.Info(_driverName + " is Initialized");
            }
            catch (BrowserNotFoundException ex)
            {
                LOGGER.Error(ex);
                throw;
            }
        }
        public static void QuitDriver()
        {
            _driver.Quit();
            _driver = null;
            LOGGER.Info(_driverName + " is Quit and Set to null");
        }
    }
}
