﻿namespace API.Response.Order
{
    public class PostOrderInvalidResponse
    {
        public int code { get; set; }
        public string type { get; set; }
        public string message { get; set; }
    }
}
