﻿using OpenQA.Selenium;

namespace UI.Pages
{
    public class LoginAndSignUpPage : BasePage
    {
        public LoginAndSignUpPage(IWebDriver driver) : base(driver)
        {

        }
        #region Elements
        private readonly By signUpNameField = By.XPath("//input[@data-qa='signup-name']");
        private readonly By signUpEmailField = By.XPath("//input[@data-qa='signup-email']");
        private readonly By signUpButton = By.XPath("//button[@data-qa='signup-button']");
        private readonly By signUpErrorMessageField = By.XPath("//input[@data-qa='signup-email']/following-sibling::p");
        private readonly By loginPasswordField = By.XPath("//input[@data-qa='login-password']");
        private readonly By loginEmailField = By.XPath("//input[@data-qa='login-email']");
        private readonly By loginButton = By.XPath("//button[@data-qa='login-button']");
        private readonly By loginErrorMessageField = By.XPath("//input[@data-qa='login-password']/following-sibling::p");

        #endregion Elements

        #region Actions
        public String GetName()
        {
            return FindElement(signUpNameField).GetAttribute("value");
        }
        public String GetEmail()
        {
            return FindElement(signUpEmailField).GetAttribute("value");
        }
        public LoginAndSignUpPage EnterSingUpName(String name)
        {
            FindElement(signUpNameField).SendKeys(name);
            return this;
        }
        public LoginAndSignUpPage EnterSignUpEmail(String email)
        {
            FindElement(signUpEmailField).SendKeys(email);
            return this;
        }
        public SignUpDetailsPage ClickOnSignUpButton()
        {
            FindElement(signUpButton).Click();
            return new SignUpDetailsPage(driver);
        }
        public String GetSignUpErrorMessage()
        {
            return FindElement(signUpErrorMessageField).Text;
        }
        public LoginAndSignUpPage EnterLoginEmail(String email)
        {
            FindElement(loginEmailField).SendKeys(email);
            return this;
        }
        public LoginAndSignUpPage EnterLoginPassword(String password)
        {
            FindElement(loginPasswordField).SendKeys(password);
            return this;
        }
        public void ClickOnLoginButton()
        {
            FindElement(loginButton).Click();
        }

        public String GetLoginErrorMessage()
        {
            return FindElement(loginErrorMessageField).Text.Trim();

        }
        public String GetLoginEmailWarningMessage()
        {
            return GetWarningMessage(FindElement(loginEmailField));
        }
        public String GetLoginPasswordWarningMessage()
        {
            return GetWarningMessage(FindElement(loginPasswordField));
        }

        #endregion
    }
}