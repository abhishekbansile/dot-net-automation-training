﻿using log4net;
using System.Runtime.CompilerServices;

namespace Utility.Shared
{
    public static class Logger
    {
        public static ILog GetLogger([CallerFilePath] string filename = "")
        {
            return LogManager.GetLogger(filename);
        }
    }
}
