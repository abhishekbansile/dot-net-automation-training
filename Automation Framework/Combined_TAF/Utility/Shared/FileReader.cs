﻿using log4net;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace Utility.Shared
{
    public static class FileReader
    {
        private static readonly ILog LOGGER = Logger.GetLogger();

        public static List<T> ReadExcelFile<T>(string filePath, string sheetName) where T : new()
        {
            List<T> data = new List<T>();
            try
            {
                if (IsFileExists(filePath))
                {
                    using (var package = new ExcelPackage(new FileInfo(filePath)))
                    {
                        ReadDataFromExcel(sheetName, data, package);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                LOGGER.Error(ex);
                throw;
            }
            catch (InvalidOperationException ex)
            {
                LOGGER.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                LOGGER.Error(ex);
                throw;
            }
            return data;
        }

        public static T ReadJSONFile<T>(string filePath)
        {
            try
            {
                string content = "";
                if (File.Exists(filePath))
                {
                    content = File.ReadAllText(filePath);
                }
                return JsonConvert.DeserializeObject<T>(content);
            }
            catch (FileNotFoundException ex)
            {
                LOGGER.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                LOGGER.Error(ex);
                throw;
            }
        }

        public static List<string> ReadTextFile(string filePath)
        {
            List<string> lines = new List<string>();
            try
            {
                if (IsFileExists(filePath))
                {
                    using (StreamReader reader = new StreamReader(filePath))
                    {
                        ReadDataFromText(lines, reader);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                LOGGER.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                LOGGER.Error(ex);
                throw;
            }
            return lines;
        }

        private static bool IsFileExists(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException($"File not found at '{filePath}'");
            }
            return true;
        }

        private static void ReadDataFromText(List<string> lines, StreamReader reader)
        {
            string? line;
            while ((line = reader.ReadLine()) != null)
            {
                lines.Add(line);
            }
        }

        private static void ReadDataFromExcel<T>(string sheetName, List<T> data, ExcelPackage package) where T : new()
        {
            var worksheet = package.Workbook.Worksheets[sheetName];
            if (worksheet == null)
            {
                throw new InvalidOperationException($"Worksheet '{sheetName}' not found in the Excel file.");
            }
            for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
            {
                T item = new T();
                for (int col = 1; col <= worksheet.Dimension.End.Column; col++)
                {
                    string? header = worksheet.Cells[1, col].Value?.ToString();
                    string? value = worksheet.Cells[row, col].Value?.ToString();
                    if (!string.IsNullOrEmpty(header))
                    {
                        var property = typeof(T).GetProperty(header);
                        if (property != null)
                        {
                            property.SetValue(item, value);
                        }
                    }
                }
                data.Add(item);
            }
        }
    }
}