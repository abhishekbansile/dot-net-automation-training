﻿namespace AutomationFramework.Support.Configuration
{
    public class Configuration
    {
        public Dev? Dev { get; set; }
        public QA? QA { get; set; }
    }

    public class Dev
    {
        public Crediantial? Crediantial { get; set; }
        public string? URL { get; set; }
        public bool IsRecording { get; set; }
        public bool IsScreenshot { get; set; }
        public Screenresolution? ScreenResolution { get; set; }
        public string? ScreenshotPath { get; set; }
        public string? LogPath { get; set; }
        public string? LogLevel { get; set; }
        public string? BrowserType { get; set; }
    }

    public class Crediantial
    {
        public string? Username { get; set; }
        public string? Password { get; set; }
    }

    public class Screenresolution
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public class QA
    {
        public Crediantial? Crediantial { get; set; }
        public string? URL { get; set; }
        public bool IsRecording { get; set; }
        public bool IsScreenshot { get; set; }
        public Screenresolution? ScreenResolution { get; set; }
        public string? ScreenshotPath { get; set; }
        public string? LogPath { get; set; }
        public string? LogLevel { get; set; }
        public string? BrowserType { get; set; }
    }
}
