﻿using OpenQA.Selenium;

namespace UI.Pages
{
    public class AccountCreatedPage : BasePage
    {
        public AccountCreatedPage(IWebDriver driver) : base(driver)
        {
        }
        #region Elements
        private readonly By accountCreatedHeading = By.XPath("//h2[@data-qa='account-created']/b");
        private readonly By accountCreatedMessage = By.XPath("//div[@class='col-sm-9 col-sm-offset-1']/p[1]");
        private readonly By continueButton = By.XPath("//a[@data-qa='continue-button']");
        #endregion Elements

        #region Actions
        public String GetAccountCreatedHeading()
        {
            return FindElement(accountCreatedHeading).Text.Trim();
        }
        public String GetAccountCreatedMessage()
        {
            return FindElement(accountCreatedMessage).Text.Trim();
        }
        public AccountCreatedPage ClickOnContinue()
        {
            FindElement(continueButton).Click();
            return this;
        }
        #endregion
    }
}