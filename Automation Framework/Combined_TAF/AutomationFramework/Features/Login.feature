﻿@UI
@DataSource:../TestData/UI/AutomationExercise.xlsx
Feature: Login

Validate Automation Exercise login Functionality

Background: Launch the browser with Base URL
    Given launch browser with base url
    When user clicks on SignUp/Login button

    @DataSet:ValidLoginDetails
    @Positive
    Scenario: Validate login functionality with valid username & password
        When user enters following Email Address and Password in Login to your account Section
            | Key      | Value         |
            | email    | <Email>       |
            | password | <Password>    |
        And user click on login button
        Then user should navigate to Home page
        And validate the Logged in as Name with users Registered Name "<Name>"
    
    @Negative
    @DataSet:InvalidLoginDetails
    Scenario: Validate login functionality with invalid username and password
        When user enters following Email Address and Password in Login to your account Section
          | Key      | Value      |
          | email    | <Email>    |
          | password | <Password> |
        And user click on login button
        Then verify error "<ErrorMessage>" should be displayed

    @DataSet:MissingLoginDetails
    @Negative
    Scenario: Validate login functionality without username and password
        When user click on login button
        Then verify that error message "<WarningMessage>" should be visible on email field on login page
        And verify that error message "<WarningMessage>" should be visible on password field on login page

    @DataSet:MissingLoginDetails
    @Negative
    Scenario: Validate login functionality with username only
        When a user enters "<Email>" as email address in Login to your account Section
        And a user click on login button
        Then verify that error message "<WarningMessage>" should be visible on password field on login page
