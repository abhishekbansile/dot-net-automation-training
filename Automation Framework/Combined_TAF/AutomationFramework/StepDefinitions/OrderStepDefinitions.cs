using API.Response.Order;
using API.Utils;
using NUnit.Framework;
using System.Net;
using Utility.Shared;

namespace AutomationFramework.StepDefinitions
{
    [Binding]
    public class OrderStepDefinitions
    {
        private ScenarioContext _scenarioContext;
        public OrderStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }
        [When(@"I send a Post request to Order end URI with the following data")]
        public void WhenISendAPostRequestToOrderEndURIWithTheFollowingData(Table table)
        {
            Dictionary<string, string> orderDetails = DataReader.ConvertTwoColTableToDictionary(table);
            var response = OrderUtils.PlaceAnOrder(orderDetails);
            _scenarioContext.Add("PostOrderValidResponse", response.Body);
            AddStatusCodeToScenarioContext(response.StatusCode);
        }

        private void AddStatusCodeToScenarioContext(HttpStatusCode statusCode)
        {
            if (_scenarioContext.ContainsKey("StatusCode"))
            {
                _scenarioContext["StatusCode"] = statusCode;
            }
            else
            {
                _scenarioContext.Add("StatusCode", statusCode);
            }
        }

        [Then(@"the response status code should be OK")]
        public void ThenTheResponseStatusCodeShouldBeOK()
        {
            Assert.AreEqual(HttpStatusCode.OK, _scenarioContext.Get<HttpStatusCode>("StatusCode"));
        }

        [Then(@"the response should have the following form data")]
        public void ThenTheResponseShouldHaveTheFollowingFormData(Table table)
        {
            Dictionary<string, string> expectedResponse = DataReader.ConvertTwoColTableToDictionary(table);
            var actualResponse = _scenarioContext.Get<PostOrderValidResponse>("PostOrderValidResponse");
            Assert.Multiple(() =>
            {
                Assert.AreEqual(int.Parse(expectedResponse["id"]), actualResponse.id);
                Assert.AreEqual(int.Parse(expectedResponse["petId"]), actualResponse.petId);
                Assert.AreEqual(int.Parse(expectedResponse["quantity"]), actualResponse.quantity);
                Assert.AreEqual(expectedResponse["shipDate"], actualResponse.shipDate);
                Assert.AreEqual(expectedResponse["status"], actualResponse.status);
                Assert.AreEqual(bool.Parse(expectedResponse["complete"]), actualResponse.complete);
            });
        }

        [When(@"I send a Post request to Order end URI without body")]
        public void WhenISendAPostRequestToOrderEndURIWithoutBody()
        {
            var response = OrderUtils.PlaceAnOrder();
            _scenarioContext.Add("PostOrderInvalidResponse", response.Body);
            AddStatusCodeToScenarioContext(response.StatusCode);
        }

        [Then(@"the response status code should be Unsupported Media Type")]
        public void ThenTheResponseStatusCodeShouldBeUnsupportedMediaType()
        {
            Assert.AreEqual(HttpStatusCode.UnsupportedMediaType,
                _scenarioContext.Get<HttpStatusCode>("StatusCode"));
        }

        [Then(@"the response should have the following data")]
        public void ThenTheResponseShouldHaveTheFollowingData(Table table)
        {
            var expectedResponse = DataReader.ConvertTwoColTableToDictionary(table);
            var actualResponse = _scenarioContext.Get<PostOrderInvalidResponse>("PostOrderInvalidResponse");
            Assert.Multiple(() =>
            {
                Assert.AreEqual(int.Parse(expectedResponse["code"]), actualResponse.code);
                Assert.AreEqual(expectedResponse["type"], actualResponse.type);
            });
        }

        [When(@"I send a Get request to Order end URI with id (.*)")]
        public void WhenISendAGetRequestToOrderEndURIWithId(int id)
        {
            var response = OrderUtils.FindPurchaseOrder(id);
            _scenarioContext.Add("GetOrderValidResponse", response.Body);
            AddStatusCodeToScenarioContext(response.StatusCode);
        }

        [Then(@"the response should have the following data as response body")]
        public void ThenTheResponseShouldHaveTheFollowingDataAsResponseBody(Table table)
        {
            var expectedResponse = DataReader.ConvertTwoColTableToDictionary(table);
            var actualResponse = _scenarioContext.Get<GetOrderValidResponse>("GetOrderValidResponse");
            Assert.Multiple(() =>
            {
                Assert.AreEqual(int.Parse(expectedResponse["id"]), actualResponse.id);
                Assert.AreEqual(int.Parse(expectedResponse["petId"]), actualResponse.petId);
                Assert.AreEqual(int.Parse(expectedResponse["quantity"]), actualResponse.quantity);
                Assert.AreEqual(expectedResponse["shipDate"], actualResponse.shipDate);
                Assert.AreEqual(expectedResponse["status"], actualResponse.status);
                Assert.AreEqual(bool.Parse(expectedResponse["complete"]), actualResponse.complete);

            });
        }

        [When(@"I send a Get request to Order end URI with Not-existent Purchase order id (.*)")]
        public void WhenISendAGetRequestToOrderEndURIWithNot_ExistentPurchaseOrderId(int id)
        {
            var response = OrderUtils.FindNonExistentPurchaseOrder(id);
            _scenarioContext.Add("GetOrderInvalidResponse", response.Body);
            AddStatusCodeToScenarioContext(response.StatusCode);
        }

        [Then(@"the response status code should be Not Found")]
        public void ThenTheResponseStatusCodeShouldBeNotFound()
        {
            Assert.AreEqual(HttpStatusCode.NotFound, _scenarioContext.Get<HttpStatusCode>("StatusCode"));
        }


        [Then(@"the response should have the following data as response  body")]
        public void ThenTheResponseShouldHaveTheFollowingDataAsResponse_Body(Table table)
        {
            var expectedResponse = DataReader.ConvertTwoColTableToDictionary(table);
            var actualResponse = _scenarioContext.Get<GetOrderInvalidResponse>("GetOrderInvalidResponse");
            Assert.Multiple(() =>
            {
                Assert.AreEqual(int.Parse(expectedResponse["code"]), actualResponse.code);
                Assert.AreEqual(expectedResponse["type"], actualResponse.type);
                string expectedMessage = expectedResponse["message"].Replace((char)160, (char)32);
                string actualMessage = actualResponse.message.Replace((char)160, (char)32);
                Assert.AreEqual(expectedMessage, actualMessage);
            });
        }
    }
}
