﻿using AutomationFramework.Drivers;
using Utility.Shared;
using Utility.Shared.Exceptions;
using static AutomationFramework.Support.Constants.Constants;

namespace AutomationFramework.Support.Configuration
{
    public class ConfigurationReader
    {
        public static void Read()
        {
            Configuration? configuration = FileReader.ReadJSONFile<Configuration>(CONFIGURATION_FILE_PATH);
            USERNAME = configuration.QA.Crediantial.Username;
            PASSWORD = configuration.QA.Crediantial.Password;
            URL = configuration.QA.URL;
            IS_RECORDING = configuration.QA.IsRecording;
            IS_SCREENSHOT = configuration.QA.IsScreenshot;
            SCREEN_RES_WIDTH = configuration.QA.ScreenResolution.Width;
            SCREEN_RES_HEIGHT = configuration.QA.ScreenResolution.Height;
            SCREENSHOT_PATH = configuration.QA.ScreenshotPath;
            LOG_PATH = configuration.QA.LogPath;
            LOG_LEVEL = configuration.QA.LogLevel;
            BROWSER_TYPE = ReadBrowserType(configuration.QA.BrowserType);
        }

        private static Driver ReadBrowserType(string? browserType)
        {
            browserType = browserType.ToUpper();
            switch (browserType)
            {
                case "CHROME":
                    return Driver.CHROME;
                case "FIREFOX":
                    return Driver.FIREFOX;
                case "EDGE":
                    return Driver.EDGE;
                default:
                    throw new InvalidBrowserNameException("Provide correct browser name in configuration file - " + CONFIGURATION_FILE_PATH);
            }
        }
    }
}
