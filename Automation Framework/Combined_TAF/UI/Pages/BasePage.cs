﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace UI.Pages
{
    public class BasePage
    {
        protected IWebDriver driver;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
        }
        protected String GetWarningMessage(IWebElement webElement)
        {
            string warningMessage = new WebDriverWait(driver, TimeSpan.FromSeconds(20))
                .Until(ExpectedConditions.ElementToBeClickable(webElement)).GetAttribute("validationMessage").Trim();

            return warningMessage;
        }
        protected IWebElement FindElement(By locator)
        {
            return driver.FindElement(locator);
        }
    }
}
