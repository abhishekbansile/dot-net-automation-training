using CalculatorUnitTests;

namespace CalculatorUnitTests
{
    [TestFixture]
    public class CalculatorTests
    {
        private Calculator calculator;

        [SetUp]
        public void Setup()
        {
            calculator = new Calculator();
        }

        [Test]
        [Category("Special")]
        [Category("Smoke")]

        public void Add_TwoNumbers()
        {
            // Arrange
            int result = calculator.Add(3, 5);

            // Act & Assert
            Assert.That(8, Is.EqualTo(result));
        }

        [Test]
        [TestCase(3, 5, ExpectedResult = 8)]
        [TestCase(-2, 7, ExpectedResult = 5)]
        [TestCase(0, 0, ExpectedResult = 0)]
        [TestCase(-5, -3, ExpectedResult = -8)]
        [Category("Addition")]
        [Category("Smoke")]
        public int Add_TwoNumbers(int a, int b)
        {
            return calculator.Add(a, b);
        }

        [Test]
        [TestCase(10, 4, ExpectedResult = 6)]
        [TestCase(-7, 2, ExpectedResult = -9)]
        [TestCase(0, 10, ExpectedResult = -10)]
        [TestCase(5, 3, ExpectedResult = 2)]
        [Category("Substraction")]
        [Category("Smoke")]
        public int Subtract_TwoNumbers(int a, int b)
        {
            return calculator.Subtract(a, b);
        }

        [Test]
        [TestCase(2, 3, ExpectedResult = 6)]
        [TestCase(-4, 5, ExpectedResult = -20)]
        [TestCase(0, 8, ExpectedResult = 0)]
        [TestCase(6, 1, ExpectedResult = 6)]
        [Category("Multiplication")]
        [Category("Smoke")]
        public int Multiply_TwoNumbers(int a, int b)
        {
            return calculator.Multiply(a, b);
        }

        [Test]
        [TestCase(8, 2, ExpectedResult = 4)]
        [TestCase(-10, 2, ExpectedResult = -5)]
        [TestCase(0, 5, ExpectedResult = 0)]
        [Category("Division")]
        [Category("Smoke")]

        public double Divide_TwoNumbers(int dividend, int divisor)
        {
            return calculator.Divide(dividend, divisor);
        }
    }
}