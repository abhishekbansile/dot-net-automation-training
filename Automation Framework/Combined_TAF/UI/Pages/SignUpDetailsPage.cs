﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace UI.Pages
{
    public class SignUpDetailsPage : BasePage
    {
        public SignUpDetailsPage(IWebDriver driver) : base(driver)
        {

        }
        #region Elements
        private readonly By nameField = By.Id("name");
        private readonly By emailField = By.Id("email");
        private readonly By mrRadioButton = By.Id("id_gender1");
        private readonly By mrsRadioButton = By.Id("id_gender2");
        private readonly By passwordField = By.Id("password");
        private readonly By daysDropDown = By.Id("days");
        private readonly By monthsDropDown = By.Id("months");
        private readonly By yearsDropDown = By.Id("years");
        private readonly By firstNameField = By.Id("first_name");
        private readonly By lastNameField = By.Id("last_name");
        private readonly By companyField = By.Id("company");
        private readonly By address1Field = By.Id("address1");
        private readonly By address2Field = By.Id("address2");
        private readonly By countryField = By.Id("country");
        private readonly By stateField = By.Id("state");
        private readonly By cityField = By.Id("city");
        private readonly By zipcodeField = By.Id("zipcode");
        private readonly By mobileNumberField = By.Id("mobile_number");
        private readonly By createAccountButton = By.XPath("//button[@data-qa='create-account']");
        private readonly By requiredFields = By.TagName("required");
        #endregion Elements

        #region Actions
        public String GetName()
        {
            return FindElement(nameField).GetAttribute("value");
        }
        public String GetEmail()
        {
            return FindElement(emailField).GetAttribute("value");
        }
        public SignUpDetailsPage SelectTitle(String title)
        {
            if (title == null)
            {
                return this;
            }
            if (title.Equals("Mr"))
            {
                FindElement(mrRadioButton).Click();
            }
            else
            {
                FindElement(mrsRadioButton).Click();
            }
            return this;
        }
        public SignUpDetailsPage EnterPassword(String password)
        {
            if (password == null)
            {
                password = "";
            }
            FindElement(passwordField).SendKeys(password);
            return this;
        }
        public SignUpDetailsPage SelectDateOfBirth(String day, String month, String year)
        {
            if (day == null | month == null | year == null)
            {
                return this;
            }
            SelectElement selectDay = new SelectElement(FindElement(daysDropDown));
            selectDay.SelectByValue(day);
            SelectElement selectMonth = new SelectElement(FindElement(monthsDropDown));
            selectMonth.SelectByText(month);
            SelectElement selectYear = new SelectElement(FindElement(yearsDropDown));
            selectYear.SelectByValue(year);
            return this;
        }
        public SignUpDetailsPage EnterFirstName(String firstName)
        {
            if (firstName == null)
            {
                firstName = "";
            }
            FindElement(firstNameField).SendKeys(firstName);
            return this;
        }
        public SignUpDetailsPage EnterLastName(String lastName)
        {
            if (lastName == null)
            {
                lastName = "";
            }
            FindElement(lastNameField).SendKeys(lastName);
            return this;
        }
        public SignUpDetailsPage EnterCompany(String company)
        {
            if (company == null)
            {
                company = "";
            }
            FindElement(companyField).SendKeys(company);
            return this;
        }
        public SignUpDetailsPage EnterAddress1(String address1)
        {
            if (address1 == null)
            {
                address1 = "";
            }
            FindElement(address1Field).SendKeys(address1);
            return this;
        }
        public SignUpDetailsPage EnterAddress2(String address2)
        {
            if (address2 == null)
            {
                address2 = "";
            }
            FindElement(address2Field).SendKeys(address2);
            return this;
        }
        public SignUpDetailsPage SelectCountry(String country)
        {
            SelectElement selectCountry = new SelectElement(FindElement(countryField));
            selectCountry.SelectByValue(country);
            return this;
        }
        public SignUpDetailsPage EnterState(String state)
        {
            if (state == null)
            {
                state = "";
            }
            FindElement(stateField).SendKeys(state);
            return this;
        }
        public SignUpDetailsPage EnterCity(String city)
        {
            if (city == null)
            {
                city = "";
            }
            FindElement(cityField).SendKeys(city);
            return this;
        }
        public SignUpDetailsPage EnterZipcode(String zipcode)
        {
            if (zipcode == null)
            {
                zipcode = "";
            }
            FindElement(zipcodeField).SendKeys(zipcode);
            return this;
        }
        public SignUpDetailsPage EnterMobileNumber(String mobileNumber)
        {
            if (mobileNumber == null)
            {
                mobileNumber = "";
            }
            FindElement(mobileNumberField).SendKeys(mobileNumber);
            return this;
        }
        public AccountCreatedPage CreateAccount()
        {
            FindElement(createAccountButton).Click();
            return new AccountCreatedPage(driver);
        }
        public String GetPasswordWarningMessage()
        {
            return GetWarningMessage(FindElement(passwordField));
        }
        public String GetFirstNameWarningMessage()
        {
            return GetWarningMessage(FindElement(firstNameField));
        }
        public String GetLastNameWarningMessage()
        {
            return GetWarningMessage(FindElement(lastNameField));
        }
        public String GetAddress1WarningMessage()
        {
            return GetWarningMessage(FindElement(address1Field));
        }
        public String GetCountryWarningMessage()
        {
            return GetWarningMessage(FindElement(countryField));
        }
        public String GetStateWarningMessage()
        {
            return GetWarningMessage(FindElement(stateField));
        }
        public String GetCityWarningMessage()
        {
            return GetWarningMessage(FindElement(cityField));
        }
        public String GetZipCodeWarningMessage()
        {
            return GetWarningMessage(FindElement(zipcodeField));
        }
        public String GetMobileNumberWarningMessage()
        {
            return GetWarningMessage(FindElement(mobileNumberField));
        }

        #endregion
    }
}