﻿using OpenQA.Selenium;
using UI.Pages;

namespace UI
{
    public class ContextInjection
    {
        public IWebDriver Driver { get; set; }
        public HomePage HomePage { get; set; }
        public LoginAndSignUpPage LoginAndSignUpPage { get; set; }
        public AccountCreatedPage AccountCreatedPage { get; set; }
        public AccountDeletedPage AccountDeletedPage { get; set; }
        public SignUpDetailsPage SignUpDetailsPage { get; set; }

        public void InitializePages(IWebDriver driver)
        {
            Driver = driver;
            HomePage = new HomePage(Driver);
            LoginAndSignUpPage = new LoginAndSignUpPage(Driver);
            AccountCreatedPage = new AccountCreatedPage(Driver);
            AccountDeletedPage = new AccountDeletedPage(Driver);
            SignUpDetailsPage = new SignUpDetailsPage(Driver);
        }
    }
}
